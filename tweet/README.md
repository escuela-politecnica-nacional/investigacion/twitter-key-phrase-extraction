# tweet

## Twitter API

### Search Api

La documentacion del `search api` se encuentra en el siguiente link:
- [Search API](https://developer.twitter.com/en/docs/tweets/search/api-reference/get-search-tweets.html)



## Description

Tweet analysis

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

