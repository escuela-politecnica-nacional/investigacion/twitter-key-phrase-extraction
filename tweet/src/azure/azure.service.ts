import {Injectable} from '@nestjs/common';
import {DocumentAzureInterface} from './interfaces/document-azure.interface';

var request = require('request');
var https = require('https');
const requestify = require('requestify');

@Injectable()
export class AzureService {
    private accessKey = '3ac7111fd5c6495b88d6fbbc93d17700';
    private uri = 'eastus.api.cognitive.microsoft.com';
    private path = '/text/analytics/v2.0/keyPhrases';
    private readonly https = https;
    private readonly requestify = requestify;

    async analizarTexto(documentos: any) {
        let contador = 1;
        console.log('Documentos', documentos.documents);
        documentos.documents = documentos.documents
            .map((doc, index) => {
                // doc.id = index;
                doc.id = contador;
                contador++;
                return doc;
            });


        // documentos = {
        //     documents: documentos.documents[0]
        // };
        console.log('documentos');
        console.log(documentos);
        return requestify
            .request('https://' + this.uri + this.path,
                {
                    method: 'POST',
                    body: documentos,
                    headers: {
                        'Ocp-Apim-Subscription-Key': '3ac7111fd5c6495b88d6fbbc93d17700',
                        'Content-Type': 'application/json'
                    },
                    dataType: 'json'
                }
            )


        // const body = JSON.stringify(documentos);
        //
        // let request_params = {
        //     method: 'POST',
        //     hostname: this.uri,
        //     path: this.path,
        //     headers: {
        //         'Ocp-Apim-Subscription-Key': this.accessKey,
        //     }
        // };
        //
        // let req = this.https.request(request_params, cb);
        // req.write(body);
        // req.end();

        // this.request
        //     .post({
        //         url: this.uri + this.path,
        //         form: {
        //             documents: documentos
        //         },
        //         headers: {
        //             'Ocp-Apim-Subscription-Key': this.accessKey,
        //             'Content-Type': 'text/json',
        //         }
        //     }, function (err, httpResponse, body) {
        //         cb(err, httpResponse, body);
        //     })

    }

}
