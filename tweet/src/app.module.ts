import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {AzureModule} from './azure/azure.module';
import {TwitterModule} from './twitter/twitter.module';

@Module({
    imports: [
        AzureModule,
        TwitterModule
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {

    constructor() {

    }
}
