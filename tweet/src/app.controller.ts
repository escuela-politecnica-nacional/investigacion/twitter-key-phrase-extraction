import {Get, Controller, Body, InternalServerErrorException, Query} from '@nestjs/common';
import {AppService} from './app.service';
import {AzureService} from './azure/azure.service';
import {DocumentAzureInterface} from './azure/interfaces/document-azure.interface';
import {TwitterApiService} from "./twitter/twitter-api.service";

@Controller()
export class AppController {
    constructor(private readonly appService: AppService,
                private readonly azureService: AzureService,
                private readonly twitterApiService: TwitterApiService) {
    }

    @Get()
    root(): string {
        return this.appService.root();
    }

    @Get('analyzarTweet')
    async analyzarTweet(@Query('palabraClave') palabraClave: string) {

        const respuestaTweets = await this.twitterApiService.buscarTweets(palabraClave);
        console.log('respuestaTweets', respuestaTweets);
        return respuestaTweets;
        // console.log('respuestaTweets');
        // const arregloTexto = respuestaTweets
        //     .statuses
        //     .map(
        //         (tweet) => {
        //             return {
        //                 text: tweet.text
        //             }
        //         }
        //     );

        // return arregloTexto;


        // const documentosLocal = {
        //     documents: arregloTexto
        // };
        // console.log(documentosLocal);
        // console.log('documentosLocal');

        // const response = await this.azureService
        //     .analizarTexto(documentosLocal);
        // const body = response.getBody();
        // const peticionCorrecta = response.code === 200;
        // console.log(response);
        // console.log('response');
        // if (peticionCorrecta) {
        //     return body;
        // } else {
        //     return new InternalServerErrorException({mensaje: 'Error en el request', error: response})
        // }
    }
}
